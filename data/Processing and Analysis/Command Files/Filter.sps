﻿USE ALL. 
COMPUTE filter_$=((MATH < 3) AND (GOOD > 2)). 
VARIABLE LABELS filter_$ '(MATH < 3) AND (GOOD > 2) (FILTER)'. 
VALUE LABELS filter_$ 0 'Not Selected' 1 'Selected'. 
FORMATS filter_$ (f1.0). 
FILTER BY filter_$. 
EXECUTE.
