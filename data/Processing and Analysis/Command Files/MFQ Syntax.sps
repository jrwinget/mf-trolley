﻿COMPUTE MFQ_HARM_AVG = MEAN(emotionally,weak,cruel,animal,kill,compassion) .
COMPUTE MFQ_FAIRNESS_AVG = MEAN(rights,unfairly,treated,justice,fairly,rich) .
COMPUTE MFQ_INGROUP_AVG = MEAN(loyalty,betray,lovecountry,team,history,family) .
COMPUTE MFQ_AUTHORITY_AVG = MEAN(traditions,respect,chaos,sexroles,soldier,kidrespect) .
COMPUTE MFQ_PURITY_AVG = MEAN(disgusting,decency,god,harmlessdg,unnatural,chastity) .
COMPUTE MFQ_PROGRESSIVISM = MEAN (MFQ_HARM_AVG, MFQ_FAIRNESS_AVG) - MEAN (MFQ_INGROUP_AVG, MFQ_AUTHORITY_AVG, MFQ_PURITY_AVG).
execute.
